/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dijkstra;
import graph_library.*;
import java.util.Map; 
/**
 *
 * @author GPU_CIC 2
 */
public class Dijkstra {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        graph_cost grafo_2= graph_generators_cost.Gnm(10,20 , false, false, "Gnm prueba");
        print_graph(grafo_2);
        graph_cost dijkstra = graph_generators_cost.dijkstra(grafo_2, 4);
        
        print_graph(dijkstra);
        graph_generators_cost.export_GV_costo(grafo_2);
        graph_generators_cost.export_GV_djisktra(dijkstra);
    }
        static void print_graph(graph_cost grafo_1)
    {
        System.out.println(grafo_1.get_name());        

        System.out.println("Nodos:");
        
        grafo_1.nodes.entrySet().forEach((entry) -> {
            System.out.println("Nodo: " + entry.getKey() + ", Nombre: " + entry.getValue().get_name() + ", Grado: " + entry.getValue().get_grade()) ;
        });
        
        System.out.println("Aristas:");
        
       grafo_1.edges_cost.entrySet().forEach((Map.Entry<String, edge_cost> entry) -> {
           System.out.println("Arista: " + entry.getKey() + ", Nombre: " + entry.getValue().get_name()+", Peso: "+String.valueOf(entry.getValue().get_cost()));
        });
    }
    
}
