/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph_library;
import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
/**
 *
 * @author GPU_CIC 2
 */
public class graph_generators_cost {
    
    static public graph_cost Gnm( int n,int m, boolean directed, boolean autocycle, String name ){
        graph_cost grafo= new graph_cost(name,directed,autocycle);

        for (int i=0;i<n;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
        }      
        int node_1;
        int node_2;
        for(int i=0;i<m;i++){
            do{
                do{
                    node_1 = (int)(Math.random()*n);
                    node_2 = (int)(Math.random()*n);    
                }while(node_1==node_2 && !autocycle);
            }while(check_rep(node_1,node_2,grafo,directed));

            grafo.edges_cost.put(node_1+","+node_2,new edge_cost(grafo.nodes.get(node_1),grafo.nodes.get(node_2),"Arista "+node_1+","+node_2));
        }
        return grafo;
    }


    static public graph_cost Gnp( int n, double p, boolean directed, boolean autocycle, String name ){
        graph_cost grafo= new graph_cost(name,directed,autocycle);

        for (int i=0;i<n;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
        }    

        for (int i=1; i<n;i++ ) {
            for (int j=0;j<i ;j++ ) {
                if(p>=Math.random()){
                    grafo.edges_cost.put(i+","+j,new edge_cost(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j));  
                }
            }
        }

        if(autocycle){
            for (int i=0;i<n;i++ ) {
                if(p>=Math.random()){
                  grafo.edges_cost.put(i+","+i,new edge_cost(grafo.nodes.get(i),grafo.nodes.get(i),"Arista "+i+","+i));  
                }
            }
        } 

        if (directed) {
             for (int j=1; j<n;j++ ) {
                for (int i=0;i<j ;i++ ) {
                    if(p>=Math.random()){
                        grafo.edges_cost.put(i+","+j,new edge_cost(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j));  
                    }
                }
            }
        } 
        return grafo;
    }

    static public graph_cost Gnr( int n, double r, boolean directed, boolean autocycle, String name ){
        graph_cost grafo= new graph_cost(name,directed,autocycle);

        ArrayList <cordinate> cordinates = new ArrayList <>();

        //double coordenadas[][]= new double [n][2];

        for (int i=0;i<n;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
            double x=Math.random();
            double y=Math.random();
            cordinates.add(new cordinate(x,y));
        }    

        for (int i=1; i<n;i++ ) {
            for (int j=0;j<i ;j++ ) {  
                double d=Math.sqrt(Math.pow(cordinates.get(i).get_x()-cordinates.get(j).get_x(),2)+Math.pow(cordinates.get(i).get_y()-cordinates.get(j).get_y(),2));
                if(d<=r){
                    grafo.edges_cost.put(i+","+j,new edge_cost(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j)); 
                    if (directed) {
                        grafo.edges_cost.put(j+","+i,new edge_cost(grafo.nodes.get(j),grafo.nodes.get(i),"Arista "+j+","+i)); 
                    }
                }
            }
        }

        if(autocycle){
            for (int i=0;i<n;i++ ) {
                  grafo.edges_cost.put(i+","+i,new edge_cost(grafo.nodes.get(i),grafo.nodes.get(i),"Arista "+i+","+i));  
            }
        } 
        return grafo;
    }    

    static public graph_cost Gnd( int n, int d, boolean directed, boolean autocycle, String name ){
        graph_cost grafo= new graph_cost(name,directed,autocycle);

        for (int i=0;i<d;i++){
            grafo.nodes.put(i,new node("Nodo "+i));
        }    

        for (int i=1; i<d;i++ ) {
            for (int j=0;j<i ;j++ ) {
                grafo.edges_cost.put(i+","+j,new edge_cost(grafo.nodes.get(i),grafo.nodes.get(j),"Arista "+i+","+j));  
           /*     if (directed) {
                    grafo.edge_costs.put(j+","+i,new edge_cost(grafo.nodes.get(j),grafo.nodes.get(i),"Arista "+j+","+i)); 
                }*/
            }
        }

        while (grafo.nodes.size()<n) {
            grafo.nodes.put(grafo.nodes.size(),new node("Nodo "+(grafo.nodes.size())));
            for (int i=0;i<grafo.nodes.size()-1 ;i++) {
                double p=1-((double)grafo.nodes.get(i).get_grade()/(double)d); 
                double r=Math.random();

                //System.out.println(i+","+String.valueOf(p)+","+String.valueOf(r));
                if (p>r) {
                    grafo.edges_cost.put(i+","+(grafo.nodes.size()-1),new edge_cost(grafo.nodes.get(i),grafo.nodes.get(grafo.nodes.size()-1),"Arista "+i+","+(grafo.nodes.size()-1)));
                }
            }
        }

       /* if(autocycle){
            for (int i=0;i<d;i++ ) {
                  grafo.edge_costs.put(i+","+i,new edge_cost(grafo.nodes.get(i),grafo.nodes.get(i),"Arista "+i+","+i));  
            }
        }*/ 
        return grafo;
    }
        
    static boolean check_rep(int node_1, int node_2,graph_cost grafo, boolean directed){
        
        boolean repeted=false;
                
        for (String key : grafo.edges_cost.keySet()) {
            
            if (key.equals(node_1+","+node_2)||key.equals(node_2+","+node_1)&&!directed) {
                 repeted=true; 
            }
        }          
        return repeted;
    }

    static public graph_cost bfs(graph_cost father, int root)

    {
        if (father.nodes.containsKey(root)) {
            graph_cost tree= new graph_cost(father.get_name()+"_bfs",father.is_directed(),father.is_autocycle());

            HashMap < Integer ,node> Layer=new HashMap<>();
            HashMap < Integer ,node> Layer_sig=new HashMap<>();
            HashMap < Integer , node > not_discovered=new HashMap<>();

            for (int key_encotered : father.nodes.keySet() ) {
                not_discovered.put(key_encotered,new node("Nodo "+key_encotered));
            }

            not_discovered.remove(root);

            Layer.put(root,new node("Nodo "+root));
            tree.nodes.put(root,new node("Nodo "+root));


            while(!Layer.isEmpty()){
                for (int key_node : Layer.keySet() ) {
                    for (String key_edge_cost : father.edges_cost.keySet()) {
                        int node_1=  father.edges_cost.get(key_edge_cost).get_node_1().get_key();
                        int node_2= father.edges_cost.get(key_edge_cost).get_node_2().get_key();
                        if (key_node==node_1&&not_discovered.containsKey(node_2)){
                            not_discovered.remove(node_2);
                            Layer_sig.put(node_2,new node("Nodo "+node_2));
                            tree.nodes.put(node_2,new node("Nodo "+node_2));
                            tree.edges_cost.put(node_1+","+node_2,new edge_cost(tree.nodes.get(node_1),tree.nodes.get(node_2),"Arista "+node_1+","+node_2));
                        }
                        if (key_node==node_2&&not_discovered.containsKey(node_1)) {
                            not_discovered.remove(node_1);
                            Layer_sig.put(node_1,new node("Nodo "+node_1));
                            tree.nodes.put(node_1,new node("Nodo "+node_1));
                            tree.edges_cost.put(node_2+","+node_1,new edge_cost(tree.nodes.get(node_2),tree.nodes.get(node_1),"Arista "+node_2+","+node_1));
                        }
                    } 
                }

                Layer.clear();

                for (int key_layer : Layer_sig.keySet() ) {
                    Layer.put(key_layer,Layer_sig.get(key_layer));
                }

                Layer_sig.clear();
            }

            return tree;      
        }
        else
        {
            return null;
        }
    }

    static public graph_cost dfs_r(graph_cost father, int root)
    {
        if (father.nodes.containsKey(root)){
            graph_cost tree= new graph_cost(father.get_name()+"_dfs_r",father.is_directed(),father.is_autocycle());
            HashMap < Integer , node > not_discovered=new HashMap<>();

            for (int key_encotered : father.nodes.keySet() ) {
                not_discovered.put(key_encotered,new node("Nodo "+key_encotered));
            }

            not_discovered.remove(root);

            tree.nodes.put(root,new node("Nodo "+root));

            dfs_recursive_call(father,root,tree,not_discovered);

            return tree;
        }
        else
        {
            return null;
        }

    }


    static private void dfs_recursive_call(graph_cost father, int node, graph_cost tree, HashMap not_discovered)
    {
        for (String key_edge_cost : father.edges_cost.keySet()) {
            int node_1=  father.edges_cost.get(key_edge_cost).get_node_1().get_key();
            int node_2= father.edges_cost.get(key_edge_cost).get_node_2().get_key();
            if (node==node_1&&not_discovered.containsKey(node_2)){
                not_discovered.remove(node_2);
                tree.nodes.put(node_2,new node("Nodo "+node_2));
                tree.edges_cost.put(node_1+","+node_2,new edge_cost(tree.nodes.get(node_1),tree.nodes.get(node_2),"Arista "+node_1+","+node_2));
                dfs_recursive_call(father,node_2,tree,not_discovered);
            }
            if (node==node_2&&not_discovered.containsKey(node_1)) {
                not_discovered.remove(node_1);
                tree.nodes.put(node_1,new node("Nodo "+node_1));
                tree.edges_cost.put(node_2+","+node_1,new edge_cost(tree.nodes.get(node_2),tree.nodes.get(node_1),"Arista "+node_2+","+node_1));
                dfs_recursive_call(father,node_1,tree,not_discovered);
            } 
        }
    }


    static public graph_cost dfs_i (graph_cost father, int root)
    {
        if (father.nodes.containsKey(root)){
            graph_cost tree= new graph_cost(father.get_name()+"_dfs_i",father.is_directed(),father.is_autocycle());
            
            HashMap < Integer , node > not_discovered=new HashMap<>();

            for (int key_encotered : father.nodes.keySet() ) {
                not_discovered.put(key_encotered,new node("Nodo "+key_encotered));
            }
            //not_discovered.remove(root);
            Stack pila = new Stack();
            
            boolean valid=false;

            par it = new par(root,0); 
            pila.push(it);

            while(!pila.empty())
            {
                it = (par) pila.pop();
                int anterior = (int)it.get_b();
                int nodo = (int)it.get_a();
                if (not_discovered.containsKey(nodo)) {
                    not_discovered.remove(nodo);
                    tree.nodes.put(nodo,new node("Nodo "+nodo));
                    if (valid) {
                        tree.edges_cost.put(anterior+","+nodo,new edge_cost(tree.nodes.get(anterior),tree.nodes.get(nodo),"Arista "+anterior+","+nodo));
                    }
                    valid=true;
                    for (String key_edge_cost : father.edges_cost.keySet())  {
                        int node_1=  father.edges_cost.get(key_edge_cost).get_node_1().get_key();
                        int node_2= father.edges_cost.get(key_edge_cost).get_node_2().get_key();
                        if (nodo==node_1){
                            pila.push(new par(node_2,node_1));
                        }
                        if (nodo==node_2&&not_discovered.containsKey(node_1)) {
                            pila.push(new par(node_1,node_2));
                        }
                    }
                }
            }
            
            return tree;
        }
        else
        {
            return null;
        }
    }




    static public graph_cost dijkstra(graph_cost father, int root)
    {
        if (father.nodes.containsKey(root)){

            graph_cost bfs_inst=bfs(father,root);

            if(father.nodes.size()==bfs_inst.nodes.size())
            {
                //System.out.println("entró");   
                graph_cost tree= new graph_cost(father.get_name()+"_dijkstra",father.is_directed(),father.is_autocycle());
                
                HashMap < Integer , node > not_discovered=new HashMap<>();
                HashMap < Object, Object >  list_of_ralaxion = new HashMap<>();

                for (int key_encotered : father.nodes.keySet() ) {
                    not_discovered.put(key_encotered,new node("Nodo "+key_encotered));
                }

                tree.nodes.put(root,new node("Nodo "+root));

                list_of_ralaxion.put(root+"",0);
                tree.nodes.get(root).set_costo(0);
                father.nodes.get(root).set_costo(0);
                not_discovered.remove(root);

                relaxion(father,list_of_ralaxion,root+"",not_discovered,root);
                //int conuter= 0;
                while(!not_discovered.isEmpty()){
                    //System.out.println("entró al bucle"); 
                //System.out.println(counter+"/n");   
                    edge_cost edge_min = get_min(list_of_ralaxion,father,not_discovered);

                    int costo = edge_min.get_cost();

                    int node_1=edge_min.get_node_1().get_key();
                    int node_2=edge_min.get_node_2().get_key();


                    if (not_discovered.containsKey(node_1)){
                        //System.out.println("entró al if 1"); 
                        not_discovered.remove(node_1);
                        tree.nodes.put(node_1,edge_min.get_node_1());
                        tree.edges_cost.put(edge_min.get_name().substring(7),edge_min);
                        costo=costo+father.nodes.get(node_2).get_costo();
                        father.nodes.get(node_1).set_costo(costo);
                        tree.nodes.get(node_1).set_costo(costo);
                        System.out.println("Seagregó el nodo: " +node_1+" costo: "+costo +" arista: "+edge_min.get_name().substring(7)); 
                        relaxion(father,list_of_ralaxion,edge_min.get_name().substring(7),not_discovered,node_1);
                    }
                    if (not_discovered.containsKey(node_2)){
                        //System.out.println("entró al if 2"); 
                        not_discovered.remove(node_2);
                        tree.nodes.put(node_2,edge_min.get_node_2());
                        tree.edges_cost.put(edge_min.get_name().substring(7),edge_min);
                        costo=costo+father.nodes.get(node_1).get_costo();
                        father.nodes.get(node_2).set_costo(costo);
                        tree.nodes.get(node_2).set_costo(costo);
                       System.out.println("Seagregó el nodo: " +node_2+" costo: "+costo+" arista: "+edge_min.get_name().substring(7)); 
                        
                        relaxion(father,list_of_ralaxion,edge_min.get_name().substring(7),not_discovered,node_2);
                    }
                }

                return tree;
            }
            else
            {
                return null;
            }
        }
        else
        {
            return null;
        }

    }

    static public void relaxion(graph_cost father, HashMap list_of_ralaxion, String edge_min, HashMap not_discovered,int nodo){
        list_of_ralaxion.remove((Object)edge_min);
        System.out.println("llamada a relaxion"); 
        father.edges_cost.keySet().forEach((key_edge_cost) -> {
            int node_1=  father.edges_cost.get(key_edge_cost).get_node_1().get_key();
            int node_2= father.edges_cost.get(key_edge_cost).get_node_2().get_key();
            if (nodo==node_1 && not_discovered.containsKey(node_2)){
                int costo = father.nodes.get(nodo).get_costo() + father.edges_cost.get(key_edge_cost).get_cost();
                list_of_ralaxion.put((Object)key_edge_cost,(Object)costo);
                //father.nodes.get(node_2).set_costo(costo);
               System.out.println("arista: "+key_edge_cost+ " costo: "+costo);
               // System.out.println("El costo agregado es"+costo); 
                
            }
            if (nodo==node_2&&not_discovered.containsKey(node_1)) {
                int costo = father.nodes.get(nodo).get_costo() + father.edges_cost.get(key_edge_cost).get_cost();
                 list_of_ralaxion.put((Object)key_edge_cost,(Object)costo);
                //father.nodes.get(node_1).set_costo(costo);
               System.out.println("arista:"+key_edge_cost+" costo: "+costo);
               // System.out.println("El costo agregado es: "+costo);
            }
        });
    }

    static public edge_cost get_min(HashMap list_of_ralaxion, graph_cost father, HashMap not_discovered)
    {
       // System.out.println("llamada a min");
        int costo=1<<30;
        par key_min= new par(0+"",costo);
        
        
        for (Object key : list_of_ralaxion.keySet() ) {
            costo=(int)list_of_ralaxion.get(key);
            int node_1=  father.edges_cost.get((String)key).get_node_1().get_key();
            int node_2= father.edges_cost.get((String)key).get_node_2().get_key();
            //System.out.println("la arista es:"+(String)key); 
            //System.out.println("el costo es:"+(int)list_of_ralaxion.get(key)); 
            if (costo<(int)key_min.get_b()&&(not_discovered.containsKey(node_1)||not_discovered.containsKey(node_2))) {
                key_min = new par((String)key,costo);

            }
            
        }
        System.out.println("la arista mínima es:" + (String)key_min.get_a() +" costo: "+(int)key_min.get_b()); 
        return father.edges_cost.get((String)key_min.get_a());
    }

    static public void export_GV_costo(graph_cost grafo){
        FileWriter fichero=null;
        PrintWriter pw;
        try
        {
            fichero = new FileWriter(grafo.get_name().replace(" ","_")+".gv");
            pw = new PrintWriter(fichero);

            String bufer;
            if (grafo.is_directed()) {
                bufer= "digraph ";
            }else{

                bufer= "graph ";
            }

            bufer = bufer + grafo.get_name().replace(" ","_");
            
            pw.println(bufer);
            pw.println("{");

            grafo.nodes.entrySet().forEach((entry) -> {
                pw.println("    "+entry.getKey()+";");
            });
 
            grafo.edges_cost.entrySet().forEach((Map.Entry<String, edge_cost> entry)  -> {

                String key=entry.getKey();
                String bufer_2;
                int i=0;

                while(key.charAt(i)!=','){
                    i++;
                }

                bufer_2="    "+key.substring(0,i);

                if (grafo.is_directed()) {
                    bufer_2=bufer_2+"->";                    
                }else{
                    bufer_2=bufer_2+"--";   
                }
                         
                bufer_2=bufer_2+key.substring(i+1);
                bufer_2 =bufer_2 +  " [label=\"";
                bufer_2 = bufer_2 + String.valueOf(grafo.edges_cost.get(key).get_cost()) +"\"];";

                pw.println(bufer_2);
            });
           pw.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }

    static public void export_GV_djisktra(graph_cost grafo){
        FileWriter fichero=null;
        PrintWriter pw;
        try
        {
            fichero = new FileWriter(grafo.get_name().replace(" ","_")+".gv");
            pw = new PrintWriter(fichero);

            String bufer;
            if (grafo.is_directed()) {
                bufer= "digraph ";
            }else{

                bufer= "graph ";
            }

            bufer = bufer + grafo.get_name().replace(" ","_");
            
            pw.println(bufer);
            pw.println("{");

            grafo.nodes.entrySet().forEach((entry) -> {

                String bufer_2 ="    "+entry.getKey();
                bufer_2=bufer_2+"[label=\""+grafo.nodes.get(entry.getKey()).get_costo()+"\"];";
                pw.println(bufer_2);
            });
 
            grafo.edges_cost.entrySet().forEach((Map.Entry<String, edge_cost> entry)  -> {

                String key=entry.getKey();
                String bufer_2;
                int i=0;

                while(key.charAt(i)!=','){
                    i++;
                }

                bufer_2="    "+key.substring(0,i);

                if (grafo.is_directed()) {
                    bufer_2=bufer_2+"->";                    
                }else{
                    bufer_2=bufer_2+"--";   
                }
                         
               // bufer_2=bufer_2+key.substring(i+1)+";";
                


                bufer_2=bufer_2+key.substring(i+1);
                bufer_2 =bufer_2 +  " [label=\"";
                bufer_2 = bufer_2 + String.valueOf(grafo.edges_cost.get(key).get_cost()) +"\"];";

                pw.println(bufer_2);
            });
           pw.println("}");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                // Nuevamente aprovechamos el finally para 
                // asegurarnos que se cierra el fichero.
                if (null != fichero)
                    fichero.close();
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }
    }
}
