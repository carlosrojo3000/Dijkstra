/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph_library;

/**
 *
 * @author GPU_CIC 2
 */
public class edge_cost extends edge{
    private  int  cost;
    public edge_cost(node node_1, node node_2, String name)
    {
        super(node_1,node_2,name);
        cost=(int) (Math.random()*1000);
    }
    
    public int get_cost(){
        return cost;
    }

    public void set_cost(int x){
    	cost = x;
    }
}
