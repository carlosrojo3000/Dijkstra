/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package graph_library;

/**
 *
 * @author GPU_CIC 2
 */
public class cordinate {
    private final double x;
    private final double y;
    
    public cordinate(double x,double y)
    {
        this.x=x;
        this.y=y;
    }
    public double get_x(){
        return x;
    }
    
    public double get_y(){
        return y;
    }
}
